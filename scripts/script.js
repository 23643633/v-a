

object=document.getElementById('getObject');                     //Find the element with the id of object
object.addEventListener('click', getObject);                     //Checks to see if object has been clicked

function getObject(){                                            //Function declared to find an object

  fetch('https://www.vam.ac.uk/api/json/museumobject/')          //Fetch the api from the appropriate link
  .then((res) => res.json())
  .then((data) => {
    let output = '<h2>Objects</h2>';                             //Adds a title for the area of data to be printed
    data.records.forEach(function(object){                       //Loops through each object, taking: Title, the object type, the artist
                                                                 //the date of the object and where it originates from
      output += `
        <ul>
          <li>Title: ${object.fields.title}</li>
          <li>Object: ${object.fields.object}</li>
          <li>Artist: ${object.fields.artist}</li>
          <li>Date: ${object.fields.date_text}</li>
          <li>Place of origin: ${object.fields.place}</li>
        </ul>
      `;
    });
    document.getElementById('output').innerHTML = output;       //Output the specified attributes onto the page in a new area
  })
}


////////////////////////////////////////////////////////


object=document.getElementById('getNecklace');
object.addEventListener('click', getNecklace);

function getNecklace(){

  fetch('https://www.vam.ac.uk/api/json/museumobject/search?objectnamesearch=necklace')
  .then((res) => res.json())
  .then((data) => {
    let output = '<h2>Necklaces</h2>';
    data.records.forEach(function(object){

      output += `
        <ul>
          <li>Title: ${object.fields.title}</li>
          <li>Object: ${object.fields.object}</li>
          <li>Artist: ${object.fields.artist}</li>
          <li>Date: ${object.fields.date_text}</li>
          <li>Place of origin: ${object.fields.place}</li>
        </ul>
      `;
    });
    document.getElementById('output').innerHTML = output;
  })
}


//////////////////////////////////////////////////////////

object=document.getElementById('getRecent');
object.addEventListener('click', getRecent);

function getRecent(){

  fetch('https://www.vam.ac.uk/api/json/museumobject/search?after=1990')
  .then((res) => res.json())
  .then((data) => {
    let output = '<h2>Recent Work</h2>';
    data.records.forEach(function(object){

      output += `
        <ul>
          <li>Title: ${object.fields.title}</li>
          <li>Object: ${object.fields.object}</li>
          <li>Artist: ${object.fields.artist}</li>
          <li>Date: ${object.fields.date_text}</li>
          <li>Place of origin: ${object.fields.place}</li>
        </ul>
      `;
    });
    document.getElementById('output').innerHTML = output;
  })
}


//////////////////////////////////////////////////////////

object=document.getElementById('getLiverpool');
object.addEventListener('click', getLiverpool);

function getLiverpool(){

  fetch('https://www.vam.ac.uk/api/json/museumobject/search?placesearch=liverpool')
  .then((res) => res.json())
  .then((data) => {
    let output = '<h2>Objects in Liverpool</h2>';
    data.records.forEach(function(object){

      output += `
        <ul>
          <li>Title: ${object.fields.title}</li>
          <li>Object: ${object.fields.object}</li>
          <li>Artist: ${object.fields.artist}</li>
          <li>Date: ${object.fields.date_text}</li>
          <li>Place of origin: ${object.fields.place}</li>
        </ul>
      `;
    });
    document.getElementById('output').innerHTML = output;
  })
}
